package main

import (
	"database/sql"
	"encoding/json"
	"io"
	"log"
	"os"
	"sort"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

type Signal struct {
	PhoneNumber   string                   `json:"phone_number"`
	User          string                   `json:"user"`
	Region        string                   `json:"region"`
	Password      string                   `json:"password"`
	Conversations map[string]*Conversation `json:"conversations"`
}

type Conversation struct {
	Members        []string  `json:"members"`
	PhoneNumbers   []string  `json:"phone_numbers"`
	NrOfMessages   int       `json:"nr_of_messages"`
	NrOfCalls      int       `json:"nr_of_calls"`
	ConversationID string    `json:"conversation_id"`
	Messages       []Message `json:"messages"`
	Calls          []Call    `json:"calls"`
}

type Message struct {
	Sender       string   `json:"sender"`
	Recipients   []string `json:"recipients"`
	Received     string   `json:"received"`
	Body         string   `json:"body"`
	SeenBy       []string `json:"seen_by"`
	Attachements []string `json:"attachements"`
}

type Call struct {
	With     string `json:"with"`
	Received string `json:"received"`
	Ended    string `json:"ended"`
	Duration int64  `json:"duration_seconds"`
	CallType string `json:"call_type"`
	Incoming bool   `json:"incoming"`
}

var (
	databaseName string = "plaintext.db"
	outfile      string = "out.json"
	db           *sql.DB
	err          error

	items  []map[string]interface{}
	conv   []map[string]interface{}
	signal Signal
)

// Constants
var (
	SQLMESSAGETABLE string = "SELECT json FROM messages;"
	SQLITEMSTABLE   string = "SELECT json FROM items"
	DBHANDLER       string = "sqlite3"

	// Signal json constants
	TIMESTAMP          string = "timestamp"
	PHONENUMBER        string = "number_id"
	UUID               string = "uuid_id"
	REGIONCODE         string = "regionCode"
	PASSWORD           string = "password"
	TYPE               string = "type"
	CALLHISTORY        string = "call-history"
	CONVERSATIONID     string = "conversationId"
	CALLHISTORYDETAILS string = "callHistoryDetails"
	ACCEPTEDTIME       string = "acceptedTime"
	ENDEDTIME          string = "endedTime"
	WASVIDEOCALL       string = "wasVideoCall"
	WASINCOMING        string = "wasIncoming"
	RECEIVEDAT         string = "received_at"
)

func main() {
	var msg []map[string]interface{}

	if db, err = sql.Open(DBHANDLER, databaseName); err != nil {
		panic(err)
	} else {
		defer db.Close()
		msg = loadFromDB(db, SQLMESSAGETABLE)
	}

	// Sort the messages in chronological order
	sort.Slice(msg, func(i, j int) bool {
		if msg[j][TIMESTAMP] != nil {
			return msg[i][TIMESTAMP].(float64) < msg[j][TIMESTAMP].(float64)
		} else {
			msg[j][TIMESTAMP] = float64(0)
			return false
		}
	})

	// Gather data about the user
	conv = loadFromDB(db, "SELECT json FROM conversations")
	items = loadFromDB(db, SQLITEMSTABLE)
	signal = Signal{
		PhoneNumber:   findInItems(items, PHONENUMBER),
		User:          findUser(items),
		Region:        findInItems(items, REGIONCODE),
		Password:      findInItems(items, PASSWORD),
		Conversations: make(map[string]*Conversation),
	}

	// Gather the message history.
	// Signal already provide json data, but this will make it easier to read
	for k := range msg {
		convID := msg[k][CONVERSATIONID].(string)
		if _, ok := signal.Conversations[convID]; !ok {
			signal.Conversations[convID] = &Conversation{
				Members:        getMembers(convID),
				PhoneNumbers:   getPhoneNumbers(convID),
				ConversationID: convID,
				NrOfMessages:   0,
				NrOfCalls:      0,
				Messages:       []Message{},
				Calls:          []Call{},
			}
		}
		if msg[k][TYPE].(string) == CALLHISTORY {
			signal.Conversations[convID].Calls = append(signal.Conversations[convID].Calls, Call{
				With:     linkIdToName(convID),
				Received: time.Unix(int64(msg[k][CALLHISTORYDETAILS].(map[string]interface{})[ACCEPTEDTIME].(float64)/1000), 0).UTC().String(),
				Ended:    time.Unix(int64(msg[k][CALLHISTORYDETAILS].(map[string]interface{})[ENDEDTIME].(float64)/1000), 0).UTC().String(),
				Duration: int64(msg[k][CALLHISTORYDETAILS].(map[string]interface{})[ENDEDTIME].(float64)/1000) - int64(msg[k][CALLHISTORYDETAILS].(map[string]interface{})[ACCEPTEDTIME].(float64)/1000),
				CallType: callType(msg[k][CALLHISTORYDETAILS].(map[string]interface{})[WASVIDEOCALL].(bool)),
				Incoming: msg[k][CALLHISTORYDETAILS].(map[string]interface{})[WASINCOMING].(bool),
			})
			signal.Conversations[convID].NrOfCalls++
		} else {
			var sender string
			var recipient []string = recipients(msg[k])
			var body string = body(msg[k])
			if msg[k][TYPE] == "outgoing" {
				sender = signal.User
			} else if msg[k][TYPE] == "message-history-unsynced" {
				sender = linkUuidToName(msg[k]["destination"].(string))
				body = "message-history-unsynced"
			} else {
				sender = linkUuidToName(msg[k]["sourceUuid"].(string))
			}
			signal.Conversations[convID].Messages = append(signal.Conversations[convID].Messages, Message{
				Sender:       sender,
				Recipients:   recipient,
				Received:     time.Unix(int64(msg[k][RECEIVEDAT].(float64)/1000), 0).UTC().String(),
				Body:         body,
				SeenBy:       seenBy(msg[k]),
				Attachements: attachements(msg[k]),
			})
			signal.Conversations[convID].NrOfMessages++
		}
	}

	// Print our findings to file
	prettyJSON, _ := json.MarshalIndent(signal, "", "    ")
	if err = WriteToFile(outfile, string(prettyJSON)); err != nil {
		log.Fatal(err)
	}
}

//getMembers get all members of a conversations based on conversation id
// TODO: Change so it works with groups
func getMembers(id string) []string {
	var ret []string = []string{"No match"}
	for _, k := range conv {
		if k["id"] == id {
			uuid := k["uuid"].(string)
			name := linkUuidToName(uuid)
			if name == "Conversation missing" {
				name = "No match"
			}
			ret = []string{name}
		}
	}
	return ret
}

// getPhoneNumbers get all phone numbers of members of a conversation
// TODO: Change so it works with groups
func getPhoneNumbers(id string) []string {
	var ret []string = []string{"No match"}
	for _, k := range conv {
		if k["id"] == id {
			ret = []string{k["e164"].(string)}
		}
	}
	return ret
}

// recipients gets who received a message
func recipients(m map[string]interface{}) []string {
	if _, ok := m["sent_to"]; ok {
		var r []string
		for _, uuid := range m["sent_to"].([]interface{}) {
			r = append(r, linkUuidToName(uuid.(string)))
		}
		return r
	} else {
		return []string{signal.User}
	}
}

// seenBy gets who has seen the message so far
func seenBy(m map[string]interface{}) []string {
	var r []string
	if _, ok := m["read_by"]; ok {
		for _, rb := range m["read_by"].([]interface{}) {
			r = append(r, rb.(string))
		}
	} else {
		r = []string{"N/D"}
	}
	return r
}

// callType is a helper function to help separate video and voice calls
func callType(wasVideo bool) string {
	if wasVideo {
		return "Video"
	} else {
		return "Voice only"
	}
}

// body gets the body of the message
func body(m map[string]interface{}) string {
	if _, ok := m["body"]; ok && m["body"] != nil {
		return m["body"].(string)
	} else {
		return "N/D"
	}
}

// attachements gets a list of the attachements sent with the message
func attachements(m map[string]interface{}) []string {
	var attach []string
	if _, ok := m["attachements"]; ok {
		for _, k := range m["attachements"].([]map[string]interface{}) {
			attach = append(attach, k["filename"].(string))
		}
	} else {
		attach = []string{}
	}
	return attach
}

// findInItems finds a unique "id" in the items table
func findInItems(items []map[string]interface{}, identifier string) string {
	for _, k := range items {
		if k["id"] == identifier {
			ret := k["value"].(string)
			ret = strings.Trim(ret, ".2")
			return ret
		}
	}
	return "N/D"
}

// findUser is an extension of findInItems as it also have to find the username
func findUser(items []map[string]interface{}) string {
	uuid := findInItems(items, UUID)
	uuid = strings.Trim(uuid, ".2")
	return linkUuidToName(uuid)
}

// loadFromDB loads a single column from the database
func loadFromDB(db *sql.DB, stmt string) []map[string]interface{} {
	var j []map[string]interface{}

	var r *sql.Rows = nil
	if r, err = db.Query(stmt); err != nil {
		panic(err)
	} else {
		defer r.Close()

		for r.Next() {
			var js string
			r.Scan(&js)

			var m map[string]interface{}
			json.Unmarshal([]byte(js), &m)
			j = append(j, m)
		}
	}
	return j
}

// linkIdToName loads the conversations table and tries to put a name to the ID
func linkIdToName(id string) string {

	for i := range conv {
		if conv[i]["id"] == id {

			// Some users may only have a username
			if conv[i]["profileName"] == nil {
				conv[i]["profileName"] = ""
			}
			if conv[i]["profileFamilyName"] == nil {
				conv[i]["profileFamilyName"] = ""
			}

			// We've recovered the username
			return conv[i]["profileName"].(string) + " " + conv[i]["profileFamilyName"].(string)
		}
	}
	// There was no matching conversation
	return "Conversation missing"
}

func linkUuidToName(id string) string {
	js := loadFromDB(db, "SELECT json FROM conversations")
	for i := range js {
		if js[i]["uuid"] == id {

			// Some users may only have a username
			if js[i]["profileName"] == nil {
				js[i]["profileName"] = ""
			}
			if js[i]["profileFamilyName"] == nil {
				js[i]["profileFamilyName"] = ""
			}

			// We've recovered the username
			return js[i]["profileName"].(string) + " " + js[i]["profileFamilyName"].(string)
		}
	}
	// There was no matching conversation
	return "No user match"
}

// WriteToFile will print any string of text to a file safely by
// checking for errors and syncing at the end.
func WriteToFile(filename string, data string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.WriteString(file, data)
	if err != nil {
		return err
	}
	return file.Sync()
}
