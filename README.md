# Signal Database Parser

A simple go program that can be used to extract messages from a Signal messenger database.

The extracted messages will have some metadata removed, and the program will try to parse Signal's jargon into usable information.

Note that it is not tested with group chats and only handles messages.

## Dependencies

You will need `github.com/mattn/go-sqlite3`. Get it by running `go get github.com/mattn/go-sqlite3`

## Usage

Unencrypt the database and dump it. Then either edit the script to point to the (unencrypted) dumped database, or call the dumped database "`plaintext.db`" and run the script in the same folder as it.

Run the script by compiling it or `go run main.go`